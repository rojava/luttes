.. index::
   pair: Luttes ; Rojava
   ! Rojava


.. _rojava_information_center:

==================================
**Rojava Information Center**
==================================


- https://rojavainformationcenter.com/
- https://www.instagram.com/rojavaic/


.. figure:: images/ric_logo.png
   :align: center
   :width: 200


.. toctree::
   :maxdepth: 6

   actions/actions
