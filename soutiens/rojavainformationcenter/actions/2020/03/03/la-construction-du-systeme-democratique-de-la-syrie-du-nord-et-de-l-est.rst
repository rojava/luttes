.. index::
   ! La construction du système démocratique de la Syrie du Nord et de l’Est

.. _construction_2020_03_03:

==========================================================================================
2020-03-03 **La construction du système démocratique de la Syrie du Nord et de l’Est**
==========================================================================================


- https://rojavainformationcenter.com/2020/03/dossier-la-construction-du-systeme-democratique-de-la-syrie-du-nord-et-de-lest/


L’invasion du territoire syrien par la Turquie, le 9 octobre 2019, a entamé
un nouveau chapitre de la Guerre Civile syrienne et ouvert de nouvelles
arènes aux jeux politiques des pouvoirs régionaux et internationaux.

Au moment où les Forces armées turques (TAF) et leurs milices franchissaient
la frontière, soutenues par des frappes aériennes et d’artillerie, il est
devenu plus urgent de répondre à la question : « Qu’est-ce qui est en jeu dans le nord et l’est de la Syrie ? »

L’attention des médias internationaux dirigée vers le nord et l’est de la Syrie
a mis en lumière la « trahison » des États-Unis envers les Forces démocratiques
syriennes (FDS) et le coût civil catastrophique de l’invasion.

Pourtant, pour bien comprendre l’assaut de la Turquie sur la Syrie du
Nord et de l’Est, il est nécessaire de comprendre le caractère social
et politique unique du système que les Forces démocratiques syriennes
se battent pour protéger.

Les institutions civiles qui composent ce système social et politique – avec lequel les FDS sont alignées –
cherchent à offrir une nouvelle orientation politique au Moyen-Orient
en présentant un modèle d’organisation qui se décrit comme révolutionnaire.

Le projet politique s’organise à travers le système
d ‘« autonomie démocratique confédéraliste », issu initialement du Mouvement
pour les droits kurdes au sein des régions à majorité kurde du Nord de
la Syrie – généralement appelé le Rojava.

Cependant, il s’est depuis élargi pour inclure les régions à majorité arabe,
ces zones ayant été libérées de l’État islamique par les FDS.
Ce projet politique a jeté les bases d’une société démocratique multi-ethnique
basée sur l’égalité de genre, la régénération écologique et un pouvoir
décentralisé et local.

Des milliers de militant·e·s, chercheurs, chercheuses et professionnel·le·s
internationales et Syrien·ne·s sont venu·e·s dans la région pour soutenir
et rejoindre le travail des institutions locales. Alors qu’elle en est
encore à ses débuts et reste ouverte à des critiques valables – en ce
qui concerne un certain nombre d’incohérences et de lacunes – la « révolution du Rojava »
a fait un bout de chemin et démontré la viabilité de ses structures.

Ce rapport décrit les structures politiques et sociales de la Syrie
du Nord et de l’Est, et le context social et historique qui les façonnent.

Nous expliquons l’évolution des  institutions depuis le développement
de l’autonomie en 2012 ainsi que l’expansion et l’adaptation de ces
institutions suite à la libération de régions auparavant sous contrôle
de l’État islamique, de 2016 à 2019. Bien que nous mettions en évidence
les écarts entre la théorie et la pratique, l’objectif de ce rapport
n’est pas d’évaluer si le projet politique en Syrie du Nord et de l’Esta
été un « succès », mais de décrire la situation telle  qu’elle  se
présente et ce qu’elle tend à devenir.

Quelques parallèles peuvent être établis avec le système des caracoles
des zapatistes au Chiapas (Mexique) et des projets à plus petite échelle
comme le projet confédéraliste municipal FEJUVE à El Alto,  en Bolivie.

Cependant, à bien des égards, le système de la Syrie du Nord et de l’Est
s’aventure dans un territoire politique inexploré.
Ayant déjà survécu plus longtemps et réalisé plus que ce que beaucoup
d’observateurs attendaient, sa trajectoire future ne peut être anticipée.
Par conséquent, une analyse du système politique doit nécessairement
s’aventurer sur le terrain de l’idéologie et de l’histoire afin de rendre
la comprehension du système plus accessible à ceux et celles qui s’y
intéressent.

L’invasion turque a menacé la survie du projet, en particulier dans les
régions occupées de Tel Abyad (Giré Spi), Seré Kaniyé  (Ras Al-Ain) et
la campagne environnante, ainsi que dans la region d’Afrin, qui a est
occupée par la Turquie et ses mercenaires depuis 2018.

Cependant, malgré une couverture médiatique acharnée cherchant à prouver
le contraire, les institutions politiques et sociales restent intactes,
fonctionnelles et autonomes dans tout le reste du nord et de l’est de
la Syrie. Les interviewé·e·s locales ont souligné leur volonté de
poursuivre le travail de construction du système politique même s’ils
et elles continuent de se défendre des attaques turques.

Au moment de la publication – deux mois après le début de l’invasion – il
y a des signes que, bien que secouées et poussées à bout, les institutions
locales et les gens continueront à développer le projet politique qui a
pris racine dans le nord et l’est de la Syrie.

:download:`Télécharger le document au format PDF <La-construction-du-système-démocratique-en-Syrie-du-Nord-et-de-lEst-Rojava.pdf>`
