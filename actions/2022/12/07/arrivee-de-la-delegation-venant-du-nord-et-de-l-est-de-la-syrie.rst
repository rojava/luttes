
.. _delegation_rojava_2022_12_07:

=========================================================================================================
2022-12-07 Arrivée de la délégation venant du nord et de l'est de la Syrie #Rojava #Internationalisme
=========================================================================================================

- https://piaille.fr/@cmoreldarleux/109472969538109220
- https://fondationdaniellemitterrand.org/le-reseau-jasmines/

Malgré le contexte actuel de guerre, la délégation venant de la région
du Nord et de l'Est de la Syrie est arrivée !

Ravie-émue de les retrouver à Lyon avec la fondation Danielle Mitterrand
et nos partenaires du réseau Jasmines, avant la suite de leur tournée
à Grenoble, Poitiers et Paris !

Plus d'informations sur Jasmines : https://fondationdaniellemitterrand.org/le-reseau-jasmines/

#Rojava #Internationalisme
