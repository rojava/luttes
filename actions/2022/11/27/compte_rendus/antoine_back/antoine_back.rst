.. index::
   pair: Antoine Back ; 2022-11-27

.. _antoine_back_2022_11_27:

=====================================================================================================================================
♀️✊🕊️ Compte rendu d'Antoine Back sur mastodon
=====================================================================================================================================

- :ref:`abkgrenoble_rojava_2022_11_25`
- https://framapiaf.org/@abkgrenoble/109416835832344007


.. figure:: antoine_back.png
   :align: center

Cet après-midi à Saint-Martin d'Hères (38) se tenait une conférence
organisée par l'Association Iséroise des Amis des Kurdes (AIAK), avec
:ref:`Berivan Firat <kurdistan_luttes:berivan_firat>` du Conseil
Démocratique Kurde en France (CDK-F) et Zoya du collectif Iran
Solidarités (https://kolektiva.social/@iranluttes)

Lutte historique pour:

- l'émancipation en Turquie (`Bakur <https://fr.wikipedia.org/wiki/Kurdistan_turc>`_),
- actuelles attaques de l'armée d'Erdogan en Syrie du nord-est (`Rojava <https://fr.wikipedia.org/wiki/Rojava>`_)
- et au nord de l'Irak (`Başûr <https://fr.wikipedia.org/wiki/Kurdistan_irakien>`_),
- répression féroce en Iran (`Rojhilat <https://fr.wikipedia.org/wiki/Kurdistan_iranien>`_).

Solidarité internationale ! Jin, jiyan, azadî ! ♀️✊🕊️



Zoya du collectif Iran Solidarité (https://kolektiva.social/@iranluttes)
============================================================================


.. figure:: ../../images/zoya.png
   :align: center


|cdkf_fr| Berivan Firat du Conseil Démocratique Kurde en France (CDK-F)
================================================================================

- https://cdkf.fr
- :ref:`kurdistan_luttes:cdk_fr`
- :ref:`kurdistan_luttes:berivan_firat`

.. figure:: ../../images/berivan_firat.png
   :align: center
