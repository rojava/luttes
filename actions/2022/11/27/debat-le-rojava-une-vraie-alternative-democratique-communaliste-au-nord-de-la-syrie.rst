.. index::
   pair: Rojava ; Kurdistan
   pair: Berivan Firat; Dimanche 27 novembre 2022

.. _debat_rojava_2022_11_27:

=====================================================================================================================================
🌄 Dimanche 27 novembre 2022 à 14h30 **débat avec Berivan Firat porte-parole du Conseil démocratique du Kurdistan** |NagihanAkarsel|
=====================================================================================================================================

- :ref:`kurdistan_luttes:berivan_firat`

#TurkeyAttacksRojava


.. figure:: images/berivan_firat.png
   :align: center

   :ref:`kurdistan_luttes:berivan_firat`


.. figure:: images/nawel_dombrowski.png
   :align: center

   Sur la photo Nawel Dombrowski, http://www.nawel-dombrowsky.com/index2.cfm?id_artiste=431502


🌹 En mémoire de Nagihan Akarsel |NagihanAkarsel|  l'une des autrices du slogan JinJiyanAzadî (kurdistan Irak) #NagihanAkarsel
==========================================================================================================================================

- :ref:`nagihan_akarsel`


Description
============


Débat avec :ref:`Berivan Firat <kurdistan_luttes:berivan_firat>` du Conseil démocratique du Kurdistan
(:ref:`Agit Polat <kurdistan_luttes:agit_polat>` était initialement annoncé mais n'a pas pu venir)

Les Kurdes du Rojava mettent-ils réellement en application le confédéralisme
démocratique avec pour piliers la démocratie directe, l’émancipation
des femmes, l’écologie, et **l’inclusion de toutes les composantes ethniques
et religieuses de la société ?**

Organisé par :ref:`l’association iséroise des amis des Kurdes (AIAK)  <kurdistan_luttes:aiak>`.


Maison de Quartier Fernand Texier
161 avenue Ambroise Croizat
Saint-Martin-d'Hères

.. _utopie_et_resistance_ref:

UTOPIE ET RÉSISTANCE - 10 jours au Nord et à l'Est de la Syrie (2021)
========================================================================

- :ref:`utopie_et_resistance_2021`


:ref:`Fil d'Antoine Back sur les sur #JASMINES et les attaques turques contre le Rojava <abkgrenoble_rojava_2022_11_25>`



Compte-rendus
===============

.. toctree::
   :maxdepth: 3

   compte_rendus/compte_rendus
