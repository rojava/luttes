.. index::
   pair: Rapport; JASMINES
   pair: Antoine Back; 2022-11-25


.. _abkgrenoble_rojava_2022_11_25:

===================================================================================================================================================
2022-11-25 Fil d'Antoine Back sur #JASMINES et les attaques turques contre le Rojava #TwitterKurds #TurkeyAttacksRojava #Kurdistan #Syria
===================================================================================================================================================

#TurkeyAttacksRojava #Kurdistan #Syria

- https://fondationdaniellemitterrand.org/wp-content/uploads/2022/11/21_07_01_rapport-NES_JASMINES_FINAL.pdf
- https://www.youtube.com/watch?v=DcgXOdNnlOU&t=2s (**UTOPIE ET RÉSISTANCE - 10 jours au Nord et à l'Est de la Syrie**)


1/10 Au printemps 2021 ..
===========================

- https://framapiaf.org/@abkgrenoble/109405103272572050
- https://fondationdaniellemitterrand.org/une-delegation-menee-par-la-fondation-danielle-mitterrand-de-retour-du-nord-et-lest-de-la-syrie/

.. figure:: images/antoine_back.png
   :align: center

   https://framapiaf.org/@abkgrenoble/109405103272572050


Au printemps 2021, une délégation menée par @FranceLibertes@twitter.com,
incluant @VilledeGrenoble@twitter.com, s’est rendue en #Syrie du Nord
et de l’Est pour rencontrer municipalités & société civile, échanger sur
les besoins des populations et les possibles coopérations.


2/10 #JASMINES est une dynamique de coopération décentralisée
=================================================================

- https://framapiaf.org/@abkgrenoble/109405103351466189
- https://fondationdaniellemitterrand.org/le-reseau-jasmines/

#JASMINES est une dynamique de coopération décentralisée entre des
municipalités et société civile de France et du Nord-Est Syrien (#NES) :
plusieurs prises de positions, événements et actions de sensibilisation
ont été menées depuis l'année dernière.

3/10 Le @GroupeURD@twitter.com a produit un rapport synthétique
==================================================================

- https://framapiaf.org/@abkgrenoble/109405103393360502
- https://fondationdaniellemitterrand.org/wp-content/uploads/2022/11/21_07_01_rapport-NES_JASMINES_FINAL.pdf

Le @GroupeURD@twitter.com a produit un rapport synthétique sur l'analyse
des besoins et les pistes de coopération entre #France et #NES, suite à
notre voyage sur place au printemps 2021, constituant une base solide
sur lequel nos collectivités peuvent travailler.



4/10 Une conférence s'est déroulée le 17 février 2022
======================================================

- https://framapiaf.org/@abkgrenoble/109405103466029044
- https://travailleur-alpin.fr/2022/02/18/grenoble-solidarite-concrete-avec-les-kurdes-de-syrie/

Une conférence s'est déroulée le 17 février 2022 à la @MI_Grenoble@twitter.com,
au cours de laquelle un compte-rendu de délégation avait été présenté
aux grenoblois·es et ami·es du peuple kurde.

Seul @Journal_LeTA@twitter.com a couvert cet événement. (merci à elles & eux)

- https://travailleur-alpin.fr/2022/02/18/grenoble-solidarite-concrete-avec-les-kurdes-de-syrie/

5/10 un film .. a été réalisé **UTOPIE ET RÉSISTANCE - 10 jours au Nord et à l'Est de la Syrie**
=================================================================================================

- https://framapiaf.org/@abkgrenoble/109405103536751697
- https://www.youtube.com/watch?v=DcgXOdNnlOU&t=2s (**UTOPIE ET RÉSISTANCE - 10 jours au Nord et à l'Est de la Syrie**)

Cette délégation #JASMINES du printemps 2021 a été documentée, un film
coproduit par la fondation @FranceLibertes@twitter.com et le @RojavaIC@twitter.com
a été réalisé.

"UTOPIE ET RÉSISTANCE - 10 jours au Nord et à l'Est de la Syrie" est visionnable en ligne ici ⤵
https://www.youtube.com/watch?v=DcgXOdNnlOU&t=2s


6/10 Aujourd'hui (25 octobre 2022) l'armée turque d'Erdogan pilonne ce territoire incroyable
================================================================================================

- https://framapiaf.org/@abkgrenoble/109405103623082160

Aujourd'hui (25 octobre 2022) l'armée turque d'Erdogan pilonne ce territoire incroyable,
où l'émancipation des femmes croise l'écologie, le coopérativisme et la
démocratie multiculturelle.

Je ne peux retenir mes larmes face aux terribles meurtrissures infligées
à cette utopie concrète.



7/10 La dignité de ces femmes & hommes face à l'adversité est une leçon
===========================================================================

- https://framapiaf.org/@abkgrenoble/109405103653858177

La dignité de ces femmes & hommes face à l'adversité est une leçon pour
nous, souvent anesthésiés par des querelles & polémiques, quelle que soit
leur légitimité ici, pouvant sembler dérisoires face aux épreuves que
nos frères & soeurs en humanité traversent ailleurs.


8/10 Le réseau #JASMINES continue à se mobiliser
=================================================

Le réseau #JASMINES continue à se mobiliser pour renforcer la coopération
internationale entre nos collectivités, la diplomatie des peuples opérant
là où celle des nations s'arrête.

Des événements se tiendront d'ailleurs prochainement à #Paris, #Lyon,
#Grenoble et #Poitiers.



9/10 Immense merci
=========================

- https://framapiaf.org/@abkgrenoble/109405103956992325


Immense merci à @JeremieChomette@twitter.com & @cmoreldarleux@twitter.com
de la fondation @FranceLibertes@twitter.com pour avoir conduit
l'initiative #JASMINES.

Merci à @EricPiolle@twitter.com & @emmanuelcarroz@twitter.com pour leur
soutien et leur confiance, malgré les risques d'une expédition en zone
de guerre de basse-intensité.



10/10 De #Grenoble à #Kobanê faisons vivre les solidarités !
================================================================

De #Grenoble à #Kobanê faisons vivre les solidarités !
Vive l'internationalisme et le municipalisme ! Bijî #Rojava !

#TwitterKurds #TurkeyAttacksRojava #Kurdistan #Syria



