.. index::
   pair: Soutien; Vendredi 28 avril 2017


.. _rojava_2017_04_28:

===================================================================================================================================================
Vendredi 28 avril 2017 **L'appel du Rojava : Soirée de soutien aux luttes du peuple kurde**
===================================================================================================================================================

- https://www.ici-grenoble.org/evenement/l-appel-du-rojava-soiree-de-soutien-aux-luttes-du-peuple-kurde

.. figure:: ../../../../images/carte_kurdistan.png
   :align: center

Introduction
================

Grande soirée de soutien aux luttes du peuple kurde avec des projections,
débats, info-kiosques, concerts rap local, bar, resto...

"Imaginez une région en pleine guerre civile appliquant le principe de
l'égalité entre hommes et femmes, une société qui transcende les clivages
communautaires, ethniques et religieux, un projet de gouvernance sociale
inédit, où les formes d'organisations autonomes marginalisent l'état.

Alors qu'au Kurdistan turque, la répression s'abat férocement sur un
peuple en lutte depuis 40 ans, au Rojava, Kurdistan de Syrie, un autre
monde devient possible.

Nous vous invitons à découvrir les luttes plurielles du peuple kurde,
pour une utopie possible, une autre façon de considérer le monde qui
peut changer l'histoire du Proche-Orient si on lui laisse une chance
de vivre, et pourquoi pas s'étendre..."

Programmation 
==============

18h 
====

Projections /débat – Au plus prés des luttes -

Les projection d'extraits de films seront suivis de discussions, avec la
participation de **l'Association Iséroise des Ami-e-s des Kurdes (AIAK)**:

- Kurdistan, rêves de printemps (extrait - 13'37) de Mikael BAUDU
  Un road movie qui nous conduit sur les traces de Gael le Ny, un photographe,
  membre des Amitiés kurdes de Bretagne.
  Avec lui et ses amis on part pour une quête à la découverte du combat
  mené par les Kurdes pour imaginer la démocratie dans leur pays.
- Kurdistan, la guerre des filles (extrait- 22') de Mylène Sauloy
  Les combattantes kurdes sont devenues le visage de la lutte contre 
  Daech et font fantasmer les médias qui relatent à l'envie leurs hauts
  faits d'armes à Kobané ou dans le Sinjar.
  Mais, au-delà de leur expertise avérée de la guérilla, les Unités de
  défense féminines sont aussi les héritières d'un mouvement de résistance,
  créé il y a près de quarante ans.
  Projet émancipateur, le mouvement des femmes kurdes porte l'utopie
  d'une société égalitaire, démocratique, multiconfessionnelle et multiethnique.
- Turquie : le parti HDP (2'32) Longtemps considéré comme le parti des
  Kurdes, le parti démocratique populaire HDP a attiré de plus en plus
  de sympathisants, jusqu'à devenir un rassemblement de l'opposition
  démocratique au régime turc. Après avoir remporté un score historique
  aux élections de 2015, mettant en danger le régime d'Erdogan, les
  membres du HDP et leurs soutiens subissent menaces, violences,
  enfermements et procès politiques.

20h
====

concerts - Le rap local est al ! :

- ♫ GAT : Rap de Grenoble, ponce le beat comme l'asphalte de ta ville.
  Tantôt trap, tantôt old school, GAT va vous livrer le premier jet de
  son projet. Textes francs du collier, la rime est anecdotique pour
  narrer la vie urbaine et ses méandres avec amour et convulsion.

- ♫ Standard : Hip Hop mélancolique

- ♫ Mosca : Pendant que l'empire tente d'absorber nos vies, détourner
  notre rage, déformer nos mots, leur rap est une invitation à aiguiser
  nos langues à la lime de nos vécus, cherchant le verbe incisif pour
  lacérer leurs propagandes et leurs illusions.

  Au delà des discours, ceci est une incitation à la subversion
  quotidienne. https://rapmosca.bandcamp.com/releases

- ♫ La Manita : Hip Hop Old School

- ♫ Artivistes : Au départ c'ést 5 potes pour qui le hip hop fait partie
  intégrante de leur vie ! Pour la plupart engagés quotidiennement dans
  différentes actions, luttes, événements etc....
  Après de long débats, échanges,apéros freestyle et différents ateliers
  d'écritures commun, l'idée de monter un projet musical se concrétise .

  Ayant des valeurs semblables, que ce soit en musique ou dans leurs
  visions globale du monde qui nous entourent, Artivistes prend forme
  et décide de préparer un première album. https://artivistes.bandcamp.com/releases

- ♫ Open mic / beat box

Les recettes de la soirée seront reversées une association agissant auprès
des victimes civiles de la guerre et de la répression.

Prix libre

De 18h à minuit

Au 38
Centre Social Autogéré
38 rue d'Alembert
Grenoble

