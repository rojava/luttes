.. index::
   pair: Luttes ; Rojava
   ! Rojava


.. raw:: html

   <a rel="me" href="https://kolektiva.social/@iranluttes"></a>
   <a rel="me" href="https://kolektiva.social/@kurdistanluttes"></a>


.. figure:: images/carte_syrie_2021.png
   :align: center
   :width: 500

   Carte de 2021, https://www.youtube.com/watch?v=DcgXOdNnlOU


.. figure:: images/carte_rojava_2016.png
   :align: center
   :width: 500

   Carte de 2016, https://fr.wikipedia.org/wiki/Rojava


|FluxWeb| `RSS <https://rojava.frama.io/luttes/rss.xml>`_

.. _rojava_luttes:

========================
🌄 Luttes au Rojava
========================

- https://fr.wikipedia.org/wiki/Rojava

.. figure:: images/logo_rojava.png
   :align: center

   Emblème du Rojava, https://fr.wikipedia.org/wiki/Rojava

.. toctree::
   :maxdepth: 6

   evenements/evenements
   actions/actions
   documentaires/documentaires
   soutiens/soutiens


