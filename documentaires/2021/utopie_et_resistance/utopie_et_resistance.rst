.. index::
   pair: Films ; Utopie et résistance


.. _utopie_et_resistance_2021:

===================================================================
**Utopie et résistance, 10 jours au Nord et à l'Est de la Syrie**
===================================================================


- https://framapiaf.org/@abkgrenoble/109405103272572050
- https://travailleur-alpin.fr/2022/02/18/grenoble-solidarite-concrete-avec-les-kurdes-de-syrie/
- https://fondationdaniellemitterrand.org/wp-content/uploads/2022/11/21_07_01_rapport-NES_JASMINES_FINAL.pdf
- https://www.youtube.com/watch?v=DcgXOdNnlOU


.. figure:: images/film_rojava.png
   :align: center

   https://www.youtube.com/watch?v=DcgXOdNnlOU (UTOPIE ET RÉSISTANCE - 10 jours au Nord et à l'Est de la Syrie)


Du 24 mai au 2 juin 2021, une délégation de représentants associatifs et
de municipalités françaises s’est rendue dans la région de Syrie du Nord
et de l’Est, pour rencontrer des acteurs et actrices locaux de la société
civile et des municipalités, échanger sur les besoins des populations et
les possibles coopérations à mettre en place dans le cadre de la dynamique
du réseau JASMINES (Jalons et Actions de Solidarité Municipalités et Internationalisme avec le Nord et l’Est de la Syrie).

Depuis 2021, la Fondation Danielle Mitterrand a impulsé une dynamique
de coopération décentralisée entre municipalités et société civile en
France et dans la région du Nord-Est de la Syrie, à travers le projet
**JASMINES**.

Cette dynamique de coopération multi-acteurs regroupe à ce stade les
villes de Paris, Lyon, Grenoble, Bordeaux, Poitiers, la métropole de Lyon,
la Fondation Danielle Mitterrand, le groupe URD, l’ONG italienne Un Ponte Per,
les Amitiés kurdes de Bretagne, les Amitiés Kurdes de Lyon, ainsi que
la coopérative Fréquence Commune.

- Pré-écriture : Fondation Danielle Mitterrand et Rojava Information Center
- Tournage : Rojava Information Center
- Montage et production : Renaud Drovin
- Sous-titrage : Renaud Drovin et Chloé Troadec
- Photographies : Christophe Thomas et Rojava Information Center

Merci aux habitant.es du Nord-Est de la Syrie et aux bâtisseuses et
bâtisseurs d’utopies ne nous avoir accordé leur précieux temps pour
partager leur dignité, leur force et leurs actions pour bâtir un futur de paix.

Merci à Berivan Omar et l’ensemble du département des municipalités et
de l’environnement du Nord et de L’est de la Syrie pour leur accueil
et l’énorme travail de préparation de la délégation.

Merci à Gulistan Sido et Chloé Troadec pour l’accompagnement et
l’incroyable traduction.

Merci à Roberta Rodriguez et l’équipe de Un Ponte Per au Nord et à l’Est
de la Syrie pour le travail de préparation et l’inspiration initiale.

Merci à Miquel Ginestà et l’équipe de du Rojava Information Center.

Merci à Khaled Issa et la représentation en France du Nord et ded l’est
de la Syrie, ainsi que les autorités du NES pour leur appui dans la réalisation de cette délégation.

Merci à Olivier Decottignies et le Consulat Général de France à Erbil
ainsi que Rewan Hussein de la représentation du Kurdistan d’Irak en
France pour leur aide précieuse.

Merci à Salih Durmus pour son accompagnement.

Merci à Agit Polat et au Centre Démocratique Kurde de France pour
ses nombreux conseils.

Merci à l’ensemble des participant.es de la délégation JASMINES.

Merci à toutes celles et ceux qui font vivre les coopérations et les
apprentissages à travers le réseau JASMINES entre le nord et l’est de la Syrie et la France.

Ce documentaire a été soutenu par La Fondation Danielle Mitterrand,
la ville de Grenoble et la ville de Paris.


